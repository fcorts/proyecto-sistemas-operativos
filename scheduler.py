import time
class Kernel(object):

    #quantum = unidad de tiempo en segundos
    def __init__(self, quantum, peripherals):
        self.quantum = quantum
        self.process_list = []
        self.T = 0
        self.lock = False
        self.peripherals = peripherals
    
    def add_process(self, p):
        while(self.lock):
            continue
        self.lock = True
        if self.T >= p.get_start_time():
            self.process_list.append(p)
            self.process_list.sort(key=lambda p: p.get_priority())
        self.lock = False

    #Simula la ejecucion de la CPU
    #aumenta el tiempo de ejecucion
    #llama al scheduler en cada iteracion
    def run(self):
        #print 'scheduler'
        while(True):
            while(self.lock):
                continue
            self.schedule()
            self.T = self.T + self.quantum
            time.sleep(self.quantum/1000.0)

    #revisa despues de cada quantum de tiempo si
    #se tiene que expropiar algun proceso
    def schedule2(self):
        if len(self.process_list) > 0:
            r = self.process_list[0].run(self.quantum)
            if r == 0:
                del self.process_list[0]

    #revisa despues de cada quantum de tiempo si
    #se tiene que expropiar algun proceso
    def schedule(self):
        if len(self.process_list) > 0:
            p = self.process_list[0]
            del self.process_list[0]
            #si el proceso p esta bloqueado, sigo con el siguiente
            #(revisar que no se meta a un loop infinito)
            while not self.check_peripherals(p):
                self.process_list.append(p)
                p = self.process_list[0]
                del self.process_list[0]

            r = p.run(self.quantum)
            if r > 0:
                self.process_list.append(p)
            else:
                for p in self.peripherals:
                    p.remove_process(p)
                    p.set_blocked(False)

    #revisa si el proceso puede ejecutarse
    def check_peripherals(self, process):
        #si el proceso usa, necesita o bloquea un periferico que se esta usando
        #retornamos falso
        for p in self.peripherals:
            if process.use_peripheral(p.name) or process.need_peripheral(p.name) or process.block_peripheral(p.name): 
                if p.get_blocked() and not process in p.get_processes():
                    return False

        #revisamos si los perifericos que usa estan siendo usados efecitvamente por el
        for p in self.peripherals:
            if process.use_peripheral(p.name) or process.need_peripheral(p.name) or process.block_peripheral(p.name): 
                if process in p.get_processes():
                    return True

        #si estamos aqui, ningun periferico que el proceso use, necesite o este bloqueado
        #esta bloqueado

        #actualizamos los perifericos que el proceso usa
        for p in self.peripherals:
            if process.use_peripheral(p.name):
                if not p.add_process(process): #si hay un solo proiceso usando el periferico (y lo necesita), verificamos prioridades
                    return False

        #actualizamos los perifericos que el proceso necesita
        for p in self.peripherals:
            if process.need_peripheral(p.name):
                p.clear_processes()
                p.add_process(process)


        #actualizamos los perifericos que el proceso bloquea
        for p in self.peripherals:
            if process.block_peripheral(p.name):
                p.clear_processes()
                p.add_process(process)
                p.set_blocked(True)

        return True



    def is_waiting(self, process):
        for p in self.peripherals:
            if process.use_peripheral(p.name) or process.need_peripheral(p.name) or process.block_peripheral(p.name): 
                if not process in p.get_processes():
                    return True
        return False


    #devuelve un string con una tabla con los
    #procesos que se estan ejecutando
    def top(self):
        print'======================================================='
        print'====================BEGIN=============================='
        print 'TOP :' + 'tiempo actual: ' + str(self.T/1000.0)
        while(self.lock):
            continue
        self.lock = True
        s = ''
        if len(self.process_list) > 0:
            s = s + 'Nombre'.ljust(25) + 'Fecha Inicio'.ljust(20) + 'Prioridad'.ljust(10) + 'Tiempo de ej'.ljust(15)+'tiempo res'.ljust(15)
            for p in self.process_list:
                s = s + '\n' + repr(p.get_name()).ljust(25) + repr(p.get_start_time()).ljust(20) + repr(p.get_priority()).ljust(10) + repr(self.T - p.get_start_time()).ljust(15)+repr(round(p.get_time())).ljust(15)
        s=s+'\n'+'======================================================='
        for i in self.peripherals:
            s=s+'\n'+i.get_name()+': '
            if i.get_blocked():
                s=s+'-bloqueado '
            if i.get_used():
                s=s+'-usando'
        self.lock = False
        return s
