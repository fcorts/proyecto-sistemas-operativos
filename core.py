# -*- coding: cp1252 -*-
import random
import peripheral
import socket
from datetime import datetime
class Process(object):

    def __init__(self, name, start_time, time, priority, process_type,peripheral):
        self.name = name    #name es el nombre del proceso
        self.time = 1000 * time    #time es el tiempo de ejecucion restante en milisegundos
        self.priority = priority #prioridad del proceso
        self.process_type = process_type #tipo de proceso (de 1 a 10)
        self.start_time = 1000 * start_time #tiempo de inicio del proceso en milisegundos
        self.blocked = False #Si el proceso esta bloqueado esperando que se desocupe algun recurso
        self.peripheral = peripheral #Hash de perifericos

    def get_name(self):
        return self.name

    def get_time(self):
        return self.time

    def get_priority(self):
        return self.priority

    def get_process_type(self):
        return self.process_type

    def get_start_time(self):
        return self.start_time
		
    def get_peripheral(self):
        return self.peripheral

    def set_peripheral(self, p):
        self.peripheral = p

    def get_blocked(self):
        return self.blocked

    def set_blocked(self, b):
        self.blocked = b

    def use_peripheral(self, k):
        return self.peripheral[k] == 1

    def need_peripheral(self, k):
        return self.peripheral[k] == 2

    def block_peripheral(self, k):
        return self.peripheral[k] == 3

    def run(self, quantum): #simula el paso de una unidad de tiempo
        self.time = self.time - quantum
        if self.time <= 0:
            self.time = 0
            self.write_file()
        return self.time

    def sendTo(self, comando, puerto): #No se si este sea el lugar correcto par colocar esta funcion. Pero envia un comando a otro proceso.
        socket_c=socket.socket()
        socket_c.connect(("127.0.0.1",puerto))
        socket_c.send(comando)
        socket_c.close()
        
    def terminate(self):
        self.time = 0

    #Crea un proceso según el string s
    #con mismo formato del archivo
    @classmethod

    def news(e,s):
        o = s.split(';')
        name = o[0]
        start_time = int(o[1])
        t = int(o[2])
        priority = int(o[3])
        cperipheral = [0,0,0,0,0,0]
        
        res = 0

        if t == 1:
            print 'asff'
            number = o[4]
            length=None;
            if o[5]=="CALL01":
                length=float("inf")
            elif o[5]=="CALL00":
                _nada=None#Alguna acci�n para terminar con la llamada. Puse el _nada=None, para evitar errores :S
            else:
                length = int(o[5])
            if length != None:
                cperipheral = [1,3,3,0,1,1]
                res = Call(name, priority, start_time, '0', number, length,t,cperipheral )
        elif t == 2:
            number = o[4]
            length=None;
            if o[5]=="CALL01":
                length=float("inf")
            elif o[5]=="CALL00":
                _nada=None#Alguna acci�n para terminar con la llamada. Puse el _nada=None, para evitar errores :S
            else:
                length = int(o[5])
            if not length==None: #Si length es None entonces es un termino de llamada, no un inicio de.
                cperipheral = [1,3,3,0,1,1]
                res = Call(name, priority, start_time, number, '0', length,t,cperipheral )
        elif t == 3:
            number = o[4]
            text = o[5]
            cperipheral = [0,1,0,0,1,1]
            res = Message(name, priority, start_time, '0', number, text,t,cperipheral)
        elif t == 4:
            number = o[4]
            text = o[5]
            cperipheral = [0,1,0,0,1,1]
            res = Message(name, priority, start_time, number, '0', text,t,cperipheral)
        elif t == 5:
            cname = o[4]
            cnum = o[5]
            cperipheral = [1,0,0,0,0,0]
            res = ContactAdd(name, priority, start_time, cname, cnum,t,cperipheral)
        else:
            if t==6:
                cperipheral=[2,1,1,1,1,1]
            if t==7:
                cperipheral=[0,0,0,1,1,0]
            if t==8:
                cperipheral=[1,0,0,1,0,0]
            if t==9:
                cperipheral=[2,1,0,1,1,1]
            if t==10:
                cperipheral=[1,1,0,0,0,0]
            time = int(o[4])
            res = Generic_Process(name, priority, start_time,time,t,cperipheral)

        peripheral = {}
        peripheral['pantalla'] = cperipheral[0]
        peripheral['audifono'] = cperipheral[1]
        peripheral['microfono'] = cperipheral[2]
        peripheral['gps'] = cperipheral[3]
        peripheral['enviarinfo'] = cperipheral[4]
        peripheral['recibirinfo'] = cperipheral[5]
        res.set_peripheral(peripheral)
        return res

        
class Call(Process):
    
    def __init__(self, name, priority,start_time,number_from, number_to, length, pt, peripheral):
        self.length = length
        self.number_from = number_from
        self.number_to = number_to
        time = length
        super(Call,self).__init__(name, start_time, time, priority, pt,peripheral)
        self.connection = None
        if number_to != '0':
            print 'creando llamada'
            self.connection=socket.socket()
            self.connection.connect(("127.0.0.1",int(number_to)))
            self.connection.send('CMD:OLI')
            self.connection.send('PR:LLamada entrante;0;2;1;1234;CALL01')
            #self.socket_c.close()

    def write_file(self):
        f = open('llamadas.txt','a')
        if self.number_from != '0':
            f.write('llamada recibida: '+self.number_from+'; tiempo :'+repr(self.length)+'; hora:'+str(datetime.now())+'\n')
        else:
            f.write('llamada realizada: '+self.number_to+'; tiempo: '+repr(self.length)+'; hora:'+str(datetime.now())+'\n')
        f.close()    

    def get_number_from(self):
        return self.number_from

    def get_number_to(self):
        return self.number_to

    def get_length(self):
        return self.length

    def run(self, quantum): #simula el paso de una unidad de tiempo
        if self.connection != None:
            self.connection.send('MSG:WENA %s'%(self.time,))
        self.time = self.time - quantum
        if self.time <= 0:
            self.time = 0
            self.write_file()
            self.connection.send('CMD:CHAO')
            self.connection.send('PR:LLamada entrante;0;2;1;1234;CALL00')
            self.connection.close()
        return self.time

class Message(Process):

    def __init__(self, name, priority,start_time, number_from, number_to, text, pt,peripheral):
        self.name = name
        self.number_from = number_from
        self.number_to = number_to
        self.text = text
        time = 20/1000.0 * len(text) # 20ms por cada caracter
        super(Message,self).__init__(name, start_time, time, priority, pt,peripheral)
        self.write_file()
        if number_to != '0':
            print 'creando mensaje'
            self.connection=socket.socket()
            self.connection.connect(("127.0.0.1",int(number_to)))
            self.connection.send('PR:Mensaje enviado;0;4;1;1234,text')

    def write_file(self):
        f = open('mensajes.txt','a')
        if self.number_from != '0':
            f.write('Mensaje recibido: '+self.number_from+'; mensaje :'+repr(self.text)+'; hora:'+str(datetime.now())+'\n')
        else:
            f.write('Mensaje enviado: '+self.number_to+'; tiempo: '+repr(self.text)+'; hora:'+str(datetime.now())+'\n')
        f.close()

    def get_number_from(self):
        return self.number_from

    def get_number_to(self):
        return self.number_to        

    def get_text(self):
        return self.text


class ContactAdd(Process):

    def __init__(self, name, priority,start_time, contact_name, contact_number, pt, peripheral):
        self.contact_number = contact_number
        self.contact_name = contact_name
        time = 20/1000.0 * (len(contact_name))
        super(ContactAdd,self).__init__(name, start_time, time, priority, pt, peripheral)
        self.write_file()

    def write_file(self):
        f = open('agenda.txt','a')
        f.write(self.contact_name+';'+repr(self.contact_number)+'\n')
        f.close()        

    def get_contact_number(self):
        return self.contact_number

    def get_contact_name(self):
        return self.contact_name

class Generic_Process(Process):

    def __init__(self, name, priority,start_time,time,pt,peripheral):
        self.time = time
        self.length = time
        super(Generic_Process,self).__init__(name, start_time, time, priority, pt,peripheral)
    def write_file(self):
        f = open('Procesos_extras.txt','a')
        f.write(self.name+': '+'; duracion:'+repr(self.length)+'; hora:'+str(datetime.now())+'\n')
        f.close()
