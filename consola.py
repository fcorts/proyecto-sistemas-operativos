# -*- coding: cp1252 -*-
from scheduler import Kernel
from core import Process
import sys,logging, time,select

try:
    import msvcrt
except:
    print''

class Consola:
    def __init__(self, kernel):
        self.kernel=kernel
    def readInput(self,caption, timeout):
        sistemaop = sys.platform
        if sistemaop =='win32' or sistemaop=='win64':
            start_time = time.time()
            print caption
            input = ''
            while True:
                if msvcrt.kbhit():
                    chr = msvcrt.getche()
                    if ord(chr) == 13: # enter_key
                        break
                    elif ord(chr) >= 32: #space_char
                        input += chr
                if len(input) == 0 and (time.time() - start_time) > timeout:
                    break
            if len(input) > 0:
                print 'sali'
                return 1
            else:
                return 0
        elif sistemaop=='linux2':
            print caption
            i, o, e = select.select( [sys.stdin], [], [], timeout )
            if (i):
                return 1
            else:
                return 0

    def iniciar(self):
        print '(si necesita ayuda digite help)'
        while(True):
            print 'ingrese comando: '
            entrada=raw_input()
            datos=entrada.split(",")
            if entrada=="top":
                aux=0
                timeout1=float(raw_input('ingrese intervalo de actualizacion de Top (en segundos)'))
                while aux==0:
                    print self.kernel.top()
                    aux= self.readInput('Presione una tecla para cerrar',timeout1)
            if entrada=="start":
                aux=raw_input('ingrese detalles del proceso (separados por ;, tiempo de inicio dejar en 1 como default)')
                p = Process.news(aux)
                self.kernel.add_process(p)
            if entrada=="exit":
                print "se cerrara el programa"
                sys.exit()
                return
            if entrada=="agenda":
                try:
                    f = open('agenda.txt','r')
                    print str(f.read())
                    f.close()
                except:
                    print 'no hay contactos aun'
            if entrada=="historial":
                try:
                    f = open('llamadas.txt','r')
                    print str(f.read())
                    f.close()
                except:
                    print 'no hay historial aun'
            if entrada=="mensajes":
                try:
                    f = open('mensajes.txt','r')
                    print str(f.read())
                    f.close()
                except:
                    print 'no hay mensajes aun'
            if entrada=="help":
                print "top: lista programas en ejecución"
                print "agenda: muestra los contactos"
                print "historial: muestra el historial de llamadas"
                print "mensajes: muestra el historial de mensajes"
                print "start Nombre, FechaEjecucion, Tipo, Prioridad, [Opciones]: Inicia el proceso con los datos entregados"
                print "exit: cerrar consola"
                print "help: mostrar información de comandos"

