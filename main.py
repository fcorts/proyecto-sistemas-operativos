from core import Process
from consola import Consola
from scheduler import Kernel
from peripheral import Peripheral
import time
import threading
import socket

kernel = 0;
processes = [];


#Thread encargado de ejecutar los procesos cuando
#corresponden segun el tiempo de llegada
def processLauncher():
    inicio=time.time()
    while(True):
        if len(processes) > 0:
            p = processes[0]
            ahora=time.time()
            if(p.get_start_time()/1000.0 < ahora-inicio):
                del processes[0]
                kernel.add_process(p)
        else:
            break
#Escucha comando extener y los ejecuta.
def serverListener(port):
    print 'empezando listener con puerto %s'%(port,)
    socket_s=socket.socket()
    socket_s.bind(("127.0.0.1",port))
    socket_s.listen(10) #con 10 creo que es mas que suficiente, no creo qu ese haga una cola mayor.
    while True:
        socket_c, dt=socket_s.accept()
        while True:
            entrante=socket_c.recv(1024)
            if not entrante:
                break
            print entrante
        newP=None
        try:
            newP=Process.news(entrante)
        except:
            print "error"
        if not newP==None:
            kernel.add_process(newP)
            


if __name__ == "__main__":


    peripherals = []
    peripherals.append(Peripheral('pantalla'))
    peripherals.append(Peripheral('audifono'))
    peripherals.append(Peripheral('microfono'))
    peripherals.append(Peripheral('gps'))
    peripherals.append(Peripheral('enviarinfo'))
    peripherals.append(Peripheral('recibirinfo'))

    print 'escribe ruta a archivo de input (input.txt)'
    path = raw_input()
    if path != '':
        f = open(path)
        a = f.read()
        f.close()
        s = a.split('\n')
        
        processes = []

        for i in s:
            if len(i)>0:
                p = Process.news(i)
                processes.append(p)
        #Ordenamos los procesos por tiempo de llegada
        processes.sort(key=lambda p:p.get_start_time())
    
    kernel=Kernel(500, peripherals)
    theConsole=Consola(kernel)
    print "Elija un numero de puerto (que representara su numero de telefono) (1324)"
    numPort=int(raw_input())
    threadInComing=threading.Thread(target=processLauncher)
    threadKernel=threading.Thread(target=kernel.run)
    threadUser=threading.Thread(target=theConsole.iniciar)
    threadServer=threading.Thread(target=serverListener,args=(numPort, )) #Esto lanza el serverListener que escucha procesos entrantes.
    threadUser.start()
    threadKernel.start()
    threadInComing.start()
    threadServer.start()
