class Peripheral(object):
    
    def __init__(self, name):
    	self.name = name
    	self.blocked = False
    	self.used = False
        self.processes = []

    def get_name(self):
    	return self.name

    def add_process(self, p):
    	#revisamos si el proceso que esta usando este periferico necesita ocuparlo
    	if len(self.processes) == 1 and self.processes[0].need_peripheral(self.name):
    		if self.processes[0].get_priority() <= p.get_priority():
    			return False
    		else:
    			self.processes[0].terminate()
    			self.processes = []

    	self.processes.append(p)
    	return True

    def remove_process(self, p):
    	if p in self.processes:
    		self.processes.remove(p)

    def clear_processes(self):
   	self.processes = []
   	self.blocked = False

    def get_processes(self):
        return self.processes

    def get_blocked(self):
    	return self.blocked

    def set_blocked(self, b):
    	self.blocked = b

    def get_used(self):
    	return self.used
    def set_used(self, b):
    	self.used = b

'''
class Pantalla(Peripheral):
	def __init__(self):
		super(Screen, self).__init__()

class Audifono(Peripheral):
	def __init__(self):
		super(Screen, self).__init__()

class Microfono(Peripheral):
	def __init__(self):
		super(Screen, self).__init__()

class GPS(Peripheral):
	def __init__(self):
		super(Screen, self).__init__()

class EnviarInfo(Peripheral):
	def __init__(self):
		super(Screen, self).__init__()

class RecibirInfo(Peripheral):
	def __init__(self):
		super(Screen, self).__init__()
'''	
